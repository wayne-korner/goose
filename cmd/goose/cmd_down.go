package main

import (
	"log"

	"bitbucket.org/goose/lib/goose"
)

var downCmd = &Command{
	Name:    "down",
	Usage:   "",
	Summary: "Roll back the version by 1",
	Help:    `down extended help here...`,
	Run:     downRun,
}

func downRun(cmd *Command, args ...string) {

	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}

	current, err := goose.GetDBVersion(conf)
	if err != nil {
		log.Fatal(err)
	}

	previous, err := goose.GetPreviousDBVersion(conf.MigrationsDir, current)
	if err != nil {
		log.Fatal(err)
	}

	if err = goose.RunMigrations(conf, conf.MigrationsDir, previous); err != nil {
		log.Fatal(err)
	}
}

var allDownCmd = &Command{
	Name:    "alldown",
	Usage:   "",
	Summary: "Roll back all migrations",
	Help:    `down extended help here...`,
	Run:     allDownRun,
}

func allDownRun(cmd *Command, args ...string) {

	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}

	for current, err := goose.GetDBVersion(conf); err == nil; {

		previous, err := goose.GetPreviousDBVersion(conf.MigrationsDir, current)
		if err != nil {

			if err == goose.ErrNoPreviousVersion {
				log.Println("Cleaning up Database with Vacuum")
				err2 := goose.Vacuum(conf)
				if err2 != nil {
					log.Fatal(err2)
				}
			}

			log.Fatal(err)
		}

		if err = goose.RunMigrations(conf, conf.MigrationsDir, previous); err != nil {
			log.Fatal(err)
		}

		current, err = goose.GetDBVersion(conf)
		if err != nil {
			log.Fatal(err)
		}

	}

}
